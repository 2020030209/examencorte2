function calculo(){

    let cantidad = document.getElementById('cantidad').value;
    let destino = document.getElementById('destino');
    let origen = document.getElementById('origen');
    let subtotal = 0;
    let comision = 0;
    let total = 0;

    if(origen.value=="1"){
        
        switch(destino.value){
            case "1":
                subtotal=(cantidad/19.85).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break;

            case "2":
                subtotal=((cantidad/19.85)*1.35).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break;

            case "3":
            subtotal=((cantidad/19.85)*0.99).toFixed(2);
            comision=(subtotal*0.03).toFixed(2);
            total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break; 

            default:
                break;    
        }

    }else if(origen.value=="2"){
        switch(destino.value){
            case "1":
                subtotal=(cantidad*19.85).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break;
            case "2":
                subtotal=(cantidad*1.35).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break;
            case "3":
            subtotal=(cantidad*0.99).toFixed(2);
            comision=(subtotal*0.03).toFixed(2);
            total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break;    
            default:
                break;    
        }

    }else if(origen.value=="3"){
        switch(destino.value){

            case "1":
                subtotal=((cantidad*19.85)/1.35).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break;

            case "2":
                subtotal=((cantidad/1.35)).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break;

            case "3":
            subtotal=((cantidad*0.99)/1.35).toFixed(2);
            comision=(subtotal*0.03).toFixed(2);
            total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break; 

            default:
                break;    
        }

    }else if(origen.value=="4"){
        switch(destino.value){
            case "1":
                subtotal=((cantidad*19.85)/0.99).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break;

            case "2":
                subtotal=(cantidad/0.99).toFixed(2);
                comision=(subtotal*0.03).toFixed(2);
                total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break;

            case "3":
            subtotal=((cantidad*1.95)/0.99).toFixed(2);
            comision=(subtotal*0.03).toFixed(2);
            total=(parseFloat(subtotal) + parseFloat(comision)).toFixed(2);
                break;
                    
            default:
                break;    
        }
    }

    document.getElementById('subtotal').value=subtotal;
    document.getElementById('comision').value=comision;
    document.getElementById('total').value=total;

}
var totalgeneral=0;
function registro(){

    let suma = document.getElementById('total').value;
    let mostrar = document.getElementById('parrafo');
    let totgeneral = document.getElementById('totalgeneral');
    
    let registro;

    var seleccion = document.getElementById('origen')[document.getElementById('origen').selectedIndex].text;
    var seleccion2 = document.getElementById('destino')[document.getElementById('destino').selectedIndex].text;

    let subtotal = document.getElementById('subtotal').value;
    let comision = document.getElementById('comision').value;
    let total = document.getElementById('total').value;
    let cantidad = document.getElementById('cantidad').value;

    registro = cantidad + " " + seleccion + " a " + seleccion2 + " " + subtotal + " "
    + comision + " " + total + "<br>";
    mostrar.innerHTML= mostrar.innerHTML + registro;

    totalgeneral =  parseFloat(suma) + totgeneral ;
    totgeneral.innerText= totalgeneral;

}

function borrar(){

        let mostrar = document.getElementById('parrafo');
        let totalgeneral= document.getElementById('totalgeneral');
        mostrar.innerHTML="<br>";
        totalgeneral.innerHTML="";

}

function quitaropc(origen){
    let destino= document.getElementById('destino');
    
    switch(origen.value){

        case "1":
        destino.options[1]=new Option('Dolar Estadounidense','1');
        destino.options[2]=new Option('Dolar Canadiense','2');
        destino.options[3]=new Option('Euro','3');
        break;

    case "2":
        destino.options[1]=new Option('Peso Mexicano','1');
        destino.options[2]=new Option('Dolar Canadiense','2');
        destino.options[3]=new Option('Euro','3');
        break;

    case "3":
        destino.options[1]=new Option('Peso Mexicano','1');
        destino.options[2]=new Option('Dolar Estadounidense','2');
        destino.options[3]=new Option('Euro','3');
        break;

    case "4":
        destino.options[1]=new Option('Peso Mexicano','1');
        destino.options[2]=new Option('Dolar Estadounidense','2');
        destino.options[3]=new Option('Dolar Canadiense','3');
        break;

    }
    }